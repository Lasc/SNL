package snl;

import javax.faces.bean.*;

@ManagedBean
public class NavigationBean {
  private String singleName = "";

  public String getSingleName() {
    return(singleName);
  }

  public void setSingleName(String singleName) {
    this.singleName = singleName;
  }
  

  public String someOtherActionControllerMethod() {
    return("index");  // Means to go to index.xhtml (since condition is not mapped in faces-config.xml)
  }
  public String singleTokenMethod()
  {
	  return("enter-name");
  }

  public String multipleTokenMethod()
  {
	  return("game-multiple-token");
  }
  
  public String startMethod(){
	  return("choose-game");
  }
  
  public String startSTAppletMethod(){
	  return("game-single-token");
  }
}
