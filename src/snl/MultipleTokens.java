package snl;

import java.applet.*;
import java.awt.*;
import java.util.*;

public class MultipleTokens extends Applet implements Runnable {

	private static final long serialVersionUID = 1L;
	private Player player1, player2, player3, currentPlayer;
	private Thread mainThread;
	private Font mainFont, boldFont;
	private Random dice;
	private Image arrow, board, window, gameover;
	private MediaTracker tracker;
	private Graphics buffer;

	private final int SIDESIZE = 10;
	private final int SQRSIZEPXLS = 40;
	private final int HGAP = 10;

	private int[] specialSquares = {4,14, 9,31, 21,42, 28,84, 51,67, 36,44, 71,91, 80,100, 
			16,6, 49,11, 47,26, 87,24, 62,19, 64,60, 56,53, 93,73, 95,75, 98,78};

	private boolean OKtoRoll = false;
	private boolean gameOver = false;
	private int diceResult = 0;

	public void init() {
		setBackground(Color.white);

		mainFont = new Font("TimesRoman", Font.PLAIN, 13);
		boldFont = new Font("TimesRoman", Font.BOLD, 14);

		player1 = new Player("Mike");
		player2 = new Player("Computer");
		player3 = new Player("Player 3");
	
		currentPlayer = player1;

		dice = new Random();

		window = createImage(500, 400);
		buffer = window.getGraphics();
		if (specialSquares.length % 2 != 0) {
			showStatus("specialSquares array has odd number of elements");
			System.exit(1);
		}

		tracker = new MediaTracker(this);
		board = getImage(getCodeBase(), "images/board3.gif");
		tracker.addImage(board, 0);

		showStatus("Loading images, please wait...");

		try {
			tracker.waitForID(0);
		} catch (InterruptedException excep) {
		}

		if (tracker.isErrorAny()) {
			showStatus("Error loading images.");
		} else if (tracker.checkAll()) {
			showStatus("Images loaded successfully.");
		}
	}

	public void start() {
		mainThread = new Thread(this);
		if (mainThread != null) {
			mainThread.start();
		}
	}

	public void update(Graphics g) {
		paint(g);
	}

	public void run() {

		repaint();

		while (!gameOver) {

			if (OKtoRoll) {

				diceResult = throwDice();
				repaint();

				try {
					Thread.sleep(300);
				} catch (Exception excep) {
				}

				currentPlayer.advanceBy(diceResult);
				if (currentPlayer.getLocation() >= 100) {

					showStatus(currentPlayer.getName() + " has won!");
					gameOver = true;
					OKtoRoll = false;
					repaint();
				} else {

					changePlayer();
				}
				OKtoRoll = false;
				repaint();

				try {
					Thread.sleep(300);
				} catch (Exception excep) {
				}

				if (querySpecialSquares()) {

					try {
						Thread.sleep(500);
					} catch (Exception excep) {
					}

					repaint();

					try {
						Thread.sleep(300);
					} catch (Exception excep) {
					}
				}

			}
			try {
				Thread.sleep(200);
			} catch (Exception excep) {
			}

		}

	}

	public void paint(Graphics g) {

		buffer.setColor(Color.white);
		buffer.fillRect(0, 0, 500, 400);
		buffer.drawImage(board, 0, 0, this);
		buffer.setColor(Color.black);
		buffer.setFont(mainFont);
		buffer.drawString("Square: " + (player1.getLocation() + 1), 420, 40);
		buffer.drawString("Square: " + (player2.getLocation() + 1), 420, 110);
		buffer.drawString("Square: " + (player3.getLocation() + 1), 420, 180);

		buffer.setFont(boldFont);
		buffer.drawString("Dice: " + diceResult, 420, 220);

		if (gameOver) {
			buffer.drawString(currentPlayer.getName(), 420, 250);
			buffer.drawString("has won!", 420, 300);
		}
		else
		{

		buffer.setColor(Color.red);
		if ((player1.getLocation() / SIDESIZE) % 2 == 1)
			buffer.fillOval(
					((SIDESIZE - (player1.getLocation() % SIDESIZE) - 1) * SQRSIZEPXLS) + 20,
					((SIDESIZE - (player1.getLocation() / SIDESIZE)) * SQRSIZEPXLS) - 35,
					15, 15);
		else
			buffer.fillOval(
					((player1.getLocation() % SIDESIZE) * SQRSIZEPXLS) + 20,
					((SIDESIZE - (player1.getLocation() / SIDESIZE)) * SQRSIZEPXLS) - 35,
					15, 15);

		buffer.drawString(player1.getName(), 420, 20);

		buffer.setColor(Color.blue);
		if ((player2.getLocation() / SIDESIZE) % 2 == 1)
			buffer.fillOval(
					((SIDESIZE - (player2.getLocation() % SIDESIZE) - 1) * SQRSIZEPXLS)	+ 20,
					((SIDESIZE - (player2.getLocation() / SIDESIZE)) * SQRSIZEPXLS) - 20,
					15, 15);
		else
			buffer.fillOval(
					((player2.getLocation() % SIDESIZE) * SQRSIZEPXLS) + 20,
					((SIDESIZE - (player2.getLocation() / SIDESIZE)) * SQRSIZEPXLS) - 20,
					15, 15);

		buffer.drawString(player2.getName(), 420, 90);

		
		buffer.setColor(Color.black);
		if ((player3.getLocation() / SIDESIZE) % 2 == 1)
			buffer.fillOval(
					((SIDESIZE - (player3.getLocation() % SIDESIZE) - 1) * SQRSIZEPXLS) + 5,
					((SIDESIZE - (player3.getLocation() / SIDESIZE)) * SQRSIZEPXLS) - 35,
					15, 15);
		else
			buffer.fillOval(
					((player3.getLocation() % SIDESIZE) * SQRSIZEPXLS) + 5 ,
					((SIDESIZE - (player3.getLocation() / SIDESIZE)) * SQRSIZEPXLS) - 35,
					15, 15);

		buffer.drawString(player3.getName(), 420, 160);
		g.drawImage(window, 0, 0, this);
		}
	}

	public int throwDice() {

		int temp = dice.nextInt() % 7;

		while (temp == 0) {
			temp = dice.nextInt() % 7;
		}

		return Math.abs(temp);
	}

	public void changePlayer() {

		if (currentPlayer == player1) {
			currentPlayer = player2;
		} else	if (currentPlayer == player2) {
			currentPlayer = player3;
		} else if (currentPlayer == player3) {
			currentPlayer = player1;
		} 
		showStatus("Current player: " + currentPlayer.getName());
	}

	public boolean keyDown(Event e, int key) {

		if (gameOver) {
			return false;

		} else if (key == 'r') {
			OKtoRoll = true;

			return true;
		} else {
			showStatus("Invalid key, use r to roll the dice.");
			return false;
		}
	}

	public boolean querySpecialSquares() {

		for (int i = 0; i < specialSquares.length - 1; i += 2) {
			if (currentPlayer.getLocation() == (specialSquares[i] - 1)) {
				currentPlayer.setLocation(specialSquares[i + 1] - 1);
				return true;
			}
		}

		return false;
	}

	public void stop() {

		if (mainThread != null) {
			mainThread.stop();
			mainThread = null;
		}
	}

}
