package snl;

public class Player {

private int location;
private String name;

public Player (String s) {
     location = 0;
     name = s;
}

public int getLocation () {

     return location;
}


public void setLocation (int newLocation) {

     location = newLocation;

     if (location >= 100) {
             location = 100;
     }
}       


public void advanceBy (int diceRoll) {

     location += diceRoll;

}


public String getName () {

     return name;
}

}